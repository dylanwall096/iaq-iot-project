<?php

header('Content-Type: application/json');

require_once 'api/functions.php';
require_once 'api/conn.php';
require_once 'api/config.php';

$db = new Functions();

$sqlQuery = "SELECT temp,Timestamp FROM BME order by Timestamp DESC";

$result = mysqli_query($conn,$sqlQuery);

$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>